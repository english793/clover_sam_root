import json
import datetime
import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

dynamodb = None

dynamodb = boto3.resource('dynamodb',  endpoint_url="http://localhost:8000/")


def dy_test_handler(event, context):
    
    table = dynamodb.Table('Test')
    
    
    
    try:
        response = table.put_item(
            Item = {
                'Key': 'test_'+ datetime.datetime.utcnow().isoformat(),
                'Date': datetime.datetime.utcnow().isoformat()
            }
        )
    except ClientError as e:
        return {
            'statusCode':500, 
            'body':e.response['Error']['Message'],
            'headers':  {
                #'Access-Control-Allow-Origin' : '*',
                #'Access-Control-Allow-Headers' : 'X-Requested-With',
                'Content-Type': 'application/json'
            }}
    else:
        
        #response = table.scan()
        #item = response['Items']
        data = {
            "message":"ok",
            'output': 'Dynamo DB 테스트 핸들러 - DB 컨트롤 성공 메시지',
            'timestamp': datetime.datetime.utcnow().isoformat()
            #'item': item
        }
        
        return {
            'statusCode':200, 
            'body':json.dumps(data),
            'headers':  {
                #'Access-Control-Allow-Origin' : '*',
                #'Access-Control-Allow-Headers' : 'X-Requested-With',
                'Content-Type': 'application/json'
            }}
            
            
    #### 기본 return
    data = {
        "message":"ok",
        'output': 'Dynamo DB 테스트 핸들러 - 기본메시지',
        'timestamp': datetime.datetime.utcnow().isoformat()
        #'item': item
    }
    
    return {
        'statusCode':200, 
        'body':json.dumps(data),
        'headers':  {
            #'Access-Control-Allow-Origin' : '*',
            #'Access-Control-Allow-Headers' : 'X-Requested-With',
            'Content-Type': 'application/json'
        }}